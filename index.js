var express = require("express");
var app = express(),
    cors = require("cors");
app.use(cors());
app.get("/", function(request, response) {
    response.send("Hello, Node.js!");
});
app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});