var field = document.getElementById("field"),
    chat = document.getElementById("chat");
var ws = new WebSocket("ws://localhost:5777/");
ws.onmessage = function(message) {
    chat.value = message.data + "\n" + chat.value;
}

ws.onopen = function() {
    field.addEventListener("keydown", function(event) {
        if (event.which == 13 && field.value.trim() != "") {
            ws.send(field.value);
            field.value = "";
        }
    });
}